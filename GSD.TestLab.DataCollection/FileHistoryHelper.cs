﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.IO;
using GSD.TestLab.DataCollection.Models;
using GSD.TestLab.DataCollection.Utilities;

namespace GSD.TestLab.DataCollection
{
    public static class FileHistoryHelper
    {
        private static void SaveFileHistory(List<FileHistory> fileHistories)
        {
            var jsonString = JsonSerializer.Serialize(fileHistories);
            File.WriteAllText(DataPaths.FileHistoryPath, jsonString);
        }
        private static string FileHistoryFileExists(string filepath)
        {
            if (!File.Exists(filepath))
            {
                var jsonString = JsonSerializer.Serialize<List<FileHistory>>(new List<FileHistory>());
                File.WriteAllText(filepath, jsonString);
            }
            return filepath;
        }

        public static List<FileHistory> GetFileHistory()
        {
            FileHistoryFileExists(DataPaths.FileHistoryPath);
            var jsonString = File.ReadAllText(DataPaths.FileHistoryPath);
            if (string.IsNullOrEmpty(jsonString))
            {
                return new List<FileHistory>();
            }
            else
            {
                return JsonSerializer.Deserialize<List<FileHistory>>(jsonString);
            }
        }

        public static void AddFileHistory(string path)
        {
            var filehistory = new FileHistory(path);
            var filehistories = GetFileHistory();

            // Overwrite previous history
            if (filehistories.Any(f => f.Equals(filehistory)))
            {
                filehistories.Remove(filehistories.First(f => f.Equals(filehistory)));
            }

            filehistories.Insert(0, filehistory);

            while (filehistories.Count > 5)
            {
                filehistories.RemoveAt(filehistories.Count - 1);
            }

            SaveFileHistory(filehistories);
        }
    }
}
