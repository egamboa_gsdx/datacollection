﻿namespace GSD.TestLab.DataCollection.Enums
{
    public enum WorklistActionType
    {
        
        MultiShot,
        WashWell,
        ReadWell,
        Other,
        NoWashDispense,
        Shaker,
        Heat,
        Prime,
        Incubation,
        
        ProbeWash,
        CaptureWellImage,
        Stirrer,
        NeedleDecon,
        ClogCheck,
        
    }
}
