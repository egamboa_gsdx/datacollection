﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSD.TestLab.DataCollection
{
    public class RuntimeResultsDataCollection
    {
        public static List<Tuple<string, TimeSpan>> GetRuntimeResults(List<string> results)
        {
            List<Tuple<string, TimeSpan>> runtimeResults = new List<Tuple<string, TimeSpan>>();
            foreach (var result in results)
            {

                runtimeResults.Add(new Tuple<string, TimeSpan>(result.Substring(0, result.LastIndexOf(";")),
                    TimeSpan.Parse(result.Substring(result.LastIndexOf(";") + 1))));
      
            }
            return runtimeResults;
        }

        public static TimeSpan GetRuntimeIncubationResults(List<string> results)
        {
            TimeSpan incubationTime = TimeSpan.Zero;
            foreach (var result in results)
            {
                if (result.Contains("Average"))
                {
                    incubationTime = incubationTime.Add(TimeSpan.Parse(result.Substring(result.IndexOf(":") + 1)));
                }
            }

            return incubationTime;
        }
    }
}
