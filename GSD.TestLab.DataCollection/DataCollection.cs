using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Documents;
using System.Windows.Media.Animation;
using GSD.TestLab.DataCollection.Enums;


namespace GSD.TestLab.DataCollection
{
    public class DataCollection
    {
        private string _delimiter = "M;";

        public static string GetWorklistName(string[] filelines)
        {
            var worklistline = filelines.SkipWhile(x => !x.StartsWith("Worklist Name"));
            return worklistline.Any() ? worklistline.First().Substring(worklistline.First().IndexOf(":") + 1) : " Error 404: Worklist Name Not Found";
        }

        public static string GetWorklistOperator(string[] filelines)
        {
            var operatorline = filelines.SkipWhile(x => !x.StartsWith("Operator"));
            return operatorline.Any() ? operatorline.First().Substring(operatorline.First().IndexOf(":") + 1) : " Error 404: Operator Name Not Found";
        }

        public static string GetNumberOfSamplesLoaded(string[] filelines)
        {
            var numsamplesloaded = filelines.SkipWhile(x => !x.StartsWith("Number of Samples"));
            return numsamplesloaded.Any() ? numsamplesloaded.First().Substring(numsamplesloaded.First().IndexOf(":") + 1) : "Error 404: Number of Samples Not Found";
        }

        public static string GetNumberOfTestRuns(string[] filelines)
        {
            var numTestRuns = filelines.SkipWhile(x => !x.StartsWith("Number of Tests to run"));
            return numTestRuns.Any() ? numTestRuns.First().Substring(numTestRuns.First().IndexOf(":") + 1) : "Error 404: Number of Tests to run Not Found";
        }

        public static List<string> GetTestNames(string[] filelines, int numTests)
        {
            var testNamesLines = filelines.SkipWhile(x => !x.StartsWith("  T")).Take(numTests).ToList();
            List<string> testNames = new List<string>();
            foreach (var line in testNamesLines)
            {
                testNames.Add(line.Substring(line.IndexOf("  ") + 1));
            }
            return testNames;
        }

        public static string GetWashBottle1(string[] filelines)
        {
            var washBottle1 = filelines.SkipWhile(x => !x.StartsWith("Wash Bottle 1"));
            return washBottle1.Any() ? washBottle1.First().Substring(washBottle1.First().IndexOf(":") + 1) : "Error 404: Wash Bottle 1 Not Found";
        }

        public static string GetWashBottle2(string[] filelines)
        {
            var washbottle2 = filelines.SkipWhile(x => !x.StartsWith("Wash Bottle 2"));
            return washbottle2.Any() ? washbottle2.First().Substring(washbottle2.First().IndexOf(":") + 1) : "Error 404: Wash Bottle 2 Not Found..jk probably not in use";
        }

        public static string GetApplicationname(string[] filelines)
        {
            var applicationName = filelines.SkipWhile(x => !x.StartsWith("Application Name"));
            return applicationName.Any() ? applicationName.First().Substring(applicationName.First().IndexOf(":") + 1) : "Error 404: Application Name Not Found";
        }

        public static string GetApplicationVersion(string[] filelines)
        {
            var applicationVersion = filelines.SkipWhile(x => !x.StartsWith("Application Version"));
            return applicationVersion.Any() ? applicationVersion.First().Substring(applicationVersion.First().IndexOf(":") + 1) : "Error 404: Application Version Not Found";
        }

        public static string GetComputerName(string[] filelines)
        {
            var pcName = filelines.SkipWhile(x => !x.StartsWith("Computer Name"));
            return pcName.Any() ? pcName.First().Substring(pcName.First().IndexOf(":") + 1) : "Error 404: Computer Name Not Found";
        }

        public static string GetInstrumentName(string[] filelines)
        {
            var instrName = filelines.SkipWhile(x => !x.StartsWith("Instrument Name"));
            return instrName.Any() ? instrName.First().Substring(instrName.First().IndexOf(":") + 2) : "Error 404: Instrument Name Not Found";
        }
        public static string GetApplicationName(string folderpath)
        {
            if (Directory.Exists(folderpath))
            {
                string[] folders = Directory.GetDirectories(folderpath);

                string[] files = Directory.GetFiles(folders[0]);

                string[] filelines = File.ReadAllLines(files[0]);

                var appName = filelines.SkipWhile(x => !x.Contains("Application Name"));

                return appName != null ? appName.First().Substring(appName.First().IndexOf(":") + 1) : string.Empty;
            }
            return string.Empty;
        }

        public static string GetInstrumentName(string folderpath)
        {
            if (Directory.Exists(folderpath))
            {
                string[] folders = Directory.GetDirectories(folderpath);

                string[] files = Directory.GetFiles(folders[0]);

                string[] filelines = File.ReadAllLines(files[0]);

                var instrName = filelines.SkipWhile(x => !x.Contains("Instrument Name"));

                return instrName != null ? instrName.First().Substring(instrName.First().IndexOf(":") + 2) : string.Empty;
            }
            return string.Empty;
        }

        public static string GetComputerName(string folderpath)
        {
            if (Directory.Exists(folderpath))
            {
                string[] folders = Directory.GetDirectories(folderpath);

                string[] files = Directory.GetFiles(folders[0]);

                string[] filelines = File.ReadAllLines(files[0]);

                var pcName = filelines.SkipWhile(x => !x.Contains("Computer Name"));

                return pcName != null ? pcName.First().Substring(pcName.First().IndexOf(": ") + 1) : string.Empty;
            }
            return string.Empty;
        }

        public TimeSpan GetTotalWorklistRunTime(List<string> filelines)
        {
            TimeSpan totaltime=TimeSpan.Zero;
            TimeSpan Screen = TimeSpan.Zero;
            TimeSpan Low = TimeSpan.Zero;
            TimeSpan High = TimeSpan.Zero;
            TimeSpan ExtraHigh = TimeSpan.Zero;
            TimeSpan Tppa = TimeSpan.Zero;
            TimeSpan RprTppa = TimeSpan.Zero;
            Screen = GetScreenTime(filelines);
            Low = GetLowTime(filelines);
            High = GetHighTime(filelines);
            ExtraHigh = GetExtraHighTime(filelines);
            Tppa = GetTppaTime(filelines);
            RprTppa = GetRprTppaTime(filelines);
            totaltime = Screen + Low + High + ExtraHigh + Tppa + RprTppa;
            

            

            return totaltime;
        }
        public TimeSpan GetTotalWorklistRunTimeAnalyzer(List<string> totalruntimestring)
        {
            TimeSpan totaltime = TimeSpan.Zero;
             DateTime starttime;
             DateTime endtime;
             

             string startline;
             string endline;
            
            
            

            
            startline = totalruntimestring.First().Substring(0, totalruntimestring.First().IndexOf(_delimiter) + 1);

             if (!DateTime.TryParse(startline, out starttime))
             {
                 _delimiter = _delimiter.Equals("M;") ? "M:" : "M;";
                 startline = totalruntimestring.First().Substring(0, totalruntimestring.First().IndexOf(_delimiter) + 1);
             }

             for (int i = 1; i < totalruntimestring.Count; i++)
             {
                 endline = totalruntimestring[i].Substring(0, totalruntimestring[i].IndexOf(_delimiter) + 1);
                 if (DateTime.TryParse(startline, out starttime))
                 {
                     if (DateTime.TryParse(endline, out endtime))
                     {
                         if (starttime > endtime)
                         {
                             totaltime += (endtime + TimeSpan.FromDays(1) - starttime);
                         }
                         else
                         {
                             totaltime += endtime - starttime;
                         }
                         startline = endline;
                     }
                 }
             }
            return totaltime;

        } 
        public int GetImageCaptureCount(List<string> filelines)
        {
            int captureCount = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(j =>j.Contains("FluidTransferAction")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
            {
                foreach (string fileline in filelines)
                {
                    if (fileline.Contains("EvaluateImage;"))
                    {
                        captureCount++;
                    }
                }
            }
            return captureCount;
        }

        // filelines.Any(s => s.Contains(x));
        /* foreach (var s in filelines){
         *  if (s.Contains(x)) { extrahighcount++};
         * }
        */


        public int GetScreenCount(List<string> filelines)
        {
            int ScreenCount=0;
            
            if(!filelines.Any(s=>s.Contains("MLI")) && !filelines.Any(d=>d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
                {
                if (!filelines.Any(e=>e.Contains("TPPA Antigen")) && filelines.Any(r=>r.Contains("Sample Screening")))
                {
                    if (!filelines.Any(e => e.Contains("1/2")) && !filelines.Any(r => r.Contains("1/32")) && !filelines.Any(r => r.Contains("1/512")))
                    {
                        ScreenCount++;

                    }
                        
          
                }
            }
            return ScreenCount;
        }
        public TimeSpan GetScreenTime(List<string> filelines)
        {
            TimeSpan ScreenTime2 = TimeSpan.Zero;
            string ScreenTime = "";
            int index = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
            {
                if (!filelines.Any(e => e.Contains("TPPA Antigen")) && filelines.Any(r => r.Contains("Sample Screening")))
                {
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("Total Runtime:"))
                        {
                            index= filelines[i].IndexOf("Total Runtime:");
                            ScreenTime = filelines[i].Substring(index+15);
                            ScreenTime2 = TimeSpan.Parse(ScreenTime);
                        }
                    }
                }
            }
            return ScreenTime2;

        }

        public int GetLowCount(List<string> filelines)
        {
            int LowCount = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
            {
                if (!filelines.Any(e => e.Contains("TPPA Antigen")) && filelines.Any(r => r.Contains("1/8")))
                {
                    LowCount++;
                }
            }
            return LowCount;
        }
        public TimeSpan GetLowTime(List<string> filelines)
        {
            TimeSpan LowTime2 = TimeSpan.Zero;
            string LowTime = "";
            int index = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
            {
                if (!filelines.Any(e => e.Contains("TPPA Antigen")) && filelines.Any(r => r.Contains("1/8")))
                {
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("Total Runtime:"))
                        {
                            index = filelines[i].IndexOf("Total Runtime:");
                            LowTime = filelines[i].Substring(index + 15);
                            LowTime2 = TimeSpan.Parse(LowTime);
                        }
                    }
                }
            }
            return LowTime2;

        }
        public int GetHighCount(List<string> filelines)
        {
            int HighCount = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
            {
                if (!filelines.Any(e => e.Contains("TPPA Antigen")) && filelines.Any(r => r.Contains("1/64")))
                {
                    HighCount++;
                }
            }
            return HighCount;
        }
        public TimeSpan GetHighTime(List<string> filelines)
        {
            TimeSpan HighTime2 = TimeSpan.Zero;
            string HighTime = "";
            int index = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
            {
                if (!filelines.Any(e => e.Contains("TPPA Antigen")) && filelines.Any(r => r.Contains("1/64")))
                {
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("Total Runtime:"))
                        {
                            index = filelines[i].IndexOf("Total Runtime:");
                            HighTime = filelines[i].Substring(index + 15);
                            HighTime2 = TimeSpan.Parse(HighTime);
                        }
                    }
                }
            }
            return HighTime2;

        }
        public int GetExtraHighCount(List<string> filelines)
        {
            int ExtraHighCount = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
            {
                if (!filelines.Any(e => e.Contains("TPPA Antigen")) && filelines.Any(r => r.Contains("1/512")))
                {
                    ExtraHighCount++;
                }
            }
            return ExtraHighCount;
        }

        public TimeSpan GetExtraHighTime(List<string> filelines)
        {
            TimeSpan ExtraHighTime2 = TimeSpan.Zero;
            string ExtraHighTime = "";
            int index = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
            {
                if (!filelines.Any(e => e.Contains("TPPA Antigen")) && filelines.Any(r => r.Contains("1/512")))
                {
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("Total Runtime:"))
                        {
                            index = filelines[i].IndexOf("Total Runtime:");
                            ExtraHighTime = filelines[i].Substring(index + 15);
                            ExtraHighTime2 = TimeSpan.Parse(ExtraHighTime);
                        }
                    }
                }
            }
            return ExtraHighTime2;
        }
            public int GetTppaCount(List<string> filelines)
        {
            int TppaCount = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Initial Prime")))
            {
                if (!filelines.Any(e => e.Contains("RPR Antigen")) && filelines.Any(r => r.Contains("TPPA Antigen")))
                {
                    TppaCount++;
                }
            }
            return TppaCount;
        }

        public TimeSpan GetTppaTime(List<string> filelines)
        {
            TimeSpan TppaTime2 = TimeSpan.Zero;
            string TppaTime = "";
            int index = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")) && filelines.Any(d1 => d1.Contains("Volume OK")))
            {
                if (!filelines.Any(e => e.Contains("RPR Antigen")) && filelines.Any(r => r.Contains("TPPA Antigen")))
                {
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("Total Runtime:"))
                        {
                            index = filelines[i].IndexOf("Total Runtime:");
                            TppaTime = filelines[i].Substring(index + 15);
                            TppaTime2 = TimeSpan.Parse(TppaTime);
                        }
                    }
                }
            }
            return TppaTime2;
        }

        public int GetRprTppaCount(List<string> filelines)
        {
            int RprTppaCount = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")))
            {
                if (filelines.Any(e => e.Contains("TPPA Antigen")) && filelines.Any(r => r.Contains("RPR Antigen")) )
                {
                    if (filelines.Any(d1 => d1.Contains("Volume OK")))
                    {
                        RprTppaCount++;
                    }
                }
            }
            return RprTppaCount;
        }

        public TimeSpan GetRprTppaTime(List<string> filelines)
        {
            TimeSpan RprTppaTime2 = TimeSpan.Zero;
            string RprTppaTime = "";
            int index = 0;
            if (!filelines.Any(s => s.Contains("MLI")) && !filelines.Any(d => d.Contains("Stopped")))
            {
                if (filelines.Any(e => e.Contains("TPPA Antigen")) && filelines.Any(r => r.Contains("RPR Antigen")) && filelines.Any(d1 => d1.Contains("Volume OK")))
                {
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("Total Runtime:"))
                        {
                            index = filelines[i].IndexOf("Total Runtime:");
                            RprTppaTime = filelines[i].Substring(index + 15);
                            RprTppaTime2 = TimeSpan.Parse(RprTppaTime);
                        }
                    }
                }
            }
            return RprTppaTime2;
        }

        public TimeSpan TotalFluidTransfer(List<string> filelines)
        {
            int index = 0;
            TimeSpan SampleTransfer = TimeSpan.Zero;
            string sampleTrans = "";
            TimeSpan FluidAntigen = TimeSpan.Zero;
            string antigenAdd = "";
            TimeSpan ControlTransfer = TimeSpan.Zero;
            string controlTrans = "";
            TimeSpan TotFluidTransfer = TimeSpan.Zero;
            for (int i = 0; i < filelines.Count; i++)
            {
                if (filelines[i].Contains("FluidTransferAction; SampleTransfer;"))
                {
                    index = filelines[i].LastIndexOf("FluidTransferAction; SampleTransfer;");
                    sampleTrans = filelines[i].Substring(index + 36);
                    SampleTransfer = TimeSpan.Parse(sampleTrans);
                }
                if (filelines[i].Contains("MultiShotFluidTransferAction; AntigenAddition;"))
                {
                    index = filelines[i].LastIndexOf("MultiShotFluidTransferAction; AntigenAddition;");
                    antigenAdd = filelines[i].Substring(index + 46);
                    FluidAntigen = TimeSpan.Parse(antigenAdd);
                }
                if (filelines[i].Contains("FluidTransferAction; ControlTransfer;"))
                {
                    index = filelines[i].LastIndexOf("FluidTransferAction; ControlTransfer;");
                    controlTrans = filelines[i].Substring(index + 37);
                    ControlTransfer = TimeSpan.Parse(controlTrans);
                }

                TotFluidTransfer = SampleTransfer + FluidAntigen + ControlTransfer;
            }
            return TotFluidTransfer;
        }


        public TimeSpan TotalIncubation(List<string> filelines)
        {
            int index = 0;
            TimeSpan Incubation = TimeSpan.Zero;
            TimeSpan NewIncubation = TimeSpan.Zero;
            string incub = "";
           
            
            for (int i = 0; i < filelines.Count; i++)
            {
                if (filelines[i].Contains("Average:"))
                {
                    index = filelines[i].IndexOf("Average:");
                    incub = filelines[i].Substring(index + 8);
                    NewIncubation = TimeSpan.Parse(incub);
                    index = 0;
                    incub = "";
                }
                Incubation = Incubation + NewIncubation;
                NewIncubation = TimeSpan.Zero;
            }
            return Incubation;
        }


        public TimeSpan TotalShaker(List<string> filelines)
        {
            int index = 0;
            TimeSpan ShakerTime = TimeSpan.Zero;
            TimeSpan NewShaker = TimeSpan.Zero;
            TimeSpan Shaker1 = TimeSpan.Zero;
            TimeSpan Shaker2 = TimeSpan.Zero;
            string incub = "";


            for (int i = 0; i < filelines.Count; i++)
            {
                if (filelines[i].Contains("State:On RPM:450"))
                {

                    incub = filelines[i].Substring(index, 8);
                    Shaker1 = TimeSpan.Parse(incub);
                    index = 0;
                    incub = "";
                    incub = filelines[i+3].Substring(index, 8);
                    Shaker2 = TimeSpan.Parse(incub);
                    if (Shaker1 > Shaker2)
                    {
                        NewShaker += (Shaker2 + TimeSpan.FromDays(1)) - Shaker1;
                    }
                    else
                    {
                        NewShaker += Shaker2 - Shaker1;
                    }

                    ShakerTime = ShakerTime + NewShaker;
                    NewShaker = TimeSpan.Zero;
                }

               
            }
            return ShakerTime;
        }
        public TimeSpan GetActionTime(List<string> filelines, WorklistActionType actionname)
        {
            DateTime starttime = new DateTime();
            DateTime endtime = new DateTime();
            TimeSpan timediff = TimeSpan.Zero;
            TimeSpan shaker = TimeSpan.Zero;
            string startline = string.Empty;
            string endline = string.Empty;

          
            switch (actionname)
            {
                /*case WorklistActionType.FluidTransfer:
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                         endline = filelines[i + 1].Substring(0, filelines[i + 1].IndexOf(_delimiter) + 1);

                         if (startline.Equals(endline))
                         {
                             if (filelines[i + 2].Contains(_delimiter))
                             {
                                 endline = filelines[i + 2].Substring(0, filelines[i + 2].IndexOf(_delimiter) + 1);
                             }
                             else
                             {
                                 endline = filelines[i + 3].Substring(0, filelines[i + 3].IndexOf(_delimiter) + 1);
                             }
                         }

                         starttime = DateTime.ParseExact(startline, "h:mm:ss tt", null);
                         endtime = DateTime.ParseExact(endline, "h:mm:ss tt", null);

                         if (starttime > endtime)
                         {
                             timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                         }
                         else
                         {
                             timediff += endtime - starttime;
                         }
                    }
                   

                    break;*/
                case WorklistActionType.WashWell:
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("WashWellAction"))
                        {
                            startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                            endline = filelines[i + 1].Substring(0, filelines[i + 1].IndexOf(_delimiter) + 1);

                            if (!filelines[i + 1].Contains("WashWellAction") && !filelines[i + 1].Contains("Worklist"))
                            {
                                if (endline.Contains(_delimiter))
                                    endline = filelines[i + 2].Substring(0, filelines[i + 2].IndexOf(_delimiter) + 1);
                                else
                                    endline = filelines[i + 3].Substring(0, filelines[i + 3].IndexOf(_delimiter) + 1);
                            }

                            starttime = DateTime.Parse(startline);
                            endtime = DateTime.Parse(endline);

                            if (starttime > endtime)
                            {
                                timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                            }
                            else
                            {
                                timediff += endtime - starttime;
                            }
                        }
                    }
                    break;
                case WorklistActionType.Shaker:
                    
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (!filelines.Any(d => d.Contains("Stopped")))
                        {
                            if (filelines[i].Contains("SleepAction"))
                            {
                                var dt = TimeSpan.Parse(filelines[i].Substring(filelines[i].Length - 8));
                                timediff += dt;
                            }
                            if (filelines[i].Contains("ShakerAction"))
                            {


                                if (filelines[i].Contains("State:On"))
                                    startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                                if (filelines[i].Contains("State:Off"))
                                {
                                    endline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);

                                    if (string.IsNullOrEmpty(startline))
                                    {
                                        endline = string.Empty;
                                    }
                                    else
                                    {
                                        starttime = DateTime.Parse(startline);
                                        endtime = DateTime.Parse(endline);
                                        if (starttime > endtime)
                                        {
                                            timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                                        }
                                        else
                                        {
                                            timediff += endtime - starttime;
                                        }
                                        startline = endline = string.Empty;
                                    }
                                }
                            }
                            if (!string.IsNullOrEmpty(startline) && string.IsNullOrEmpty(endline) && i == filelines.Count)
                            {
                                endline = filelines.Last().Substring(0, filelines.Last().IndexOf(_delimiter) + 1);
                                starttime = DateTime.Parse(startline);
                                endtime = DateTime.Parse(endline);

                                if (starttime > endtime)
                                {
                                    timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                                }
                                else
                                {
                                    timediff += endtime - starttime;
                                }
                                startline = endline = string.Empty;
                            }
                        }
                            
                    }
                    break;
                case WorklistActionType.Heat:
                    int numtests = 0;
                    var stringnum = filelines.SkipWhile(x => !x.Contains("Number of Tests to run:")).ToList();
                    numtests = stringnum.Count == 0 ? 1 : Int32.Parse(stringnum[0].Substring(stringnum[0].IndexOf(": ") + 1));

                    if (numtests == 1)
                    {
                        for (int i = 0; i < filelines.Count; i++)
                        {
                            if (filelines[i].Contains("HeaterAction") && filelines[i].Contains("State:On"))
                            {
                                startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                                starttime = DateTime.Parse(startline);
                            }

                            if (filelines[i].Contains("HeaterAction") && filelines[i].Contains("State:Off"))
                            {
                                endline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                                endtime = DateTime.Parse(endline);

                                if (starttime > endtime)
                                {
                                    timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                                }
                                else
                                {
                                    timediff += endtime - starttime;
                                }
                                startline = endline = string.Empty;
                            }
                            if (!string.IsNullOrEmpty(startline) && string.IsNullOrEmpty(endline) && i == filelines.Count)
                            {
                                endline = filelines.Last().Substring(0, filelines.Last().IndexOf(_delimiter) + 1);

                                endtime = DateTime.Parse(endline);

                                if (starttime > endtime)
                                {
                                    timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                                }
                                else
                                {
                                    timediff += endtime - starttime;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= numtests; i++)
                        {
                            for (int l = 0; l < filelines.Count; l++)
                            {
                                if (filelines[l].Contains(string.Format("T{0}", i)))
                                {
                                    if (filelines[l].Contains("HeaterAction") && filelines[l].Contains("State:On"))
                                    {
                                        startline = filelines[l].Substring(0, filelines[l].IndexOf(_delimiter) + 1);
                                        starttime = DateTime.Parse(startline);
                                    }
                                    if (filelines[l].Contains("HeaterAction") && filelines[l].Contains("State:Off"))
                                    {
                                        endline = filelines[l].Substring(0, filelines[l].IndexOf(_delimiter) + 1);

                                        endtime = DateTime.Parse(endline);
                                        if (starttime > endtime)
                                        {
                                            timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                                        }
                                        else
                                        {
                                            timediff += endtime - starttime;
                                        }
                                        startline = endline = string.Empty;
                                    }
                                }

                                if (!string.IsNullOrEmpty(startline) && string.IsNullOrEmpty(endline) && i == filelines.Count)
                                {
                                    endline = filelines.Last().Substring(0, filelines.Last().IndexOf(_delimiter) + 1);
                                    endtime = DateTime.Parse(endline);

                                    if (starttime > endtime)
                                    {
                                        timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                                    }
                                    else
                                    {
                                        timediff += endtime - starttime;
                                    }
                                }
                            }

                            
                        }
                    }
                    break;
                case WorklistActionType.ReadWell:
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("ReadWellAction"))
                        {
                            if (filelines[i].Contains("Read Well") || filelines[i].Contains("Read,"))
                            {
                                startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                            }

                            if (filelines[i].Contains("Result of Read"))
                            {
                                endline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                            }

                            if (!string.IsNullOrEmpty(startline) && !string.IsNullOrEmpty(endline))
                            {
                                starttime = DateTime.Parse(startline);
                                endtime = DateTime.Parse(endline);

                                if (starttime > endtime)
                                {
                                    timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                                }
                                else
                                {
                                    timediff += endtime - starttime;
                                }

                                startline = string.Empty;
                                endline = string.Empty;
                            }
                        }
                    }
                    break;
                case WorklistActionType.Prime:
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("Prime"))
                        {
                            startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);

                            endline = filelines[i + 1].Substring(0, filelines[i + 1].IndexOf(_delimiter) + 1);

                            starttime = DateTime.Parse(startline);
                            endtime = DateTime.Parse(endline);

                            if (starttime > endtime)
                            {
                                timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                            }
                            else
                            {
                                timediff += endtime - starttime;
                            }
                        }
                    }
                    break;
                case WorklistActionType.Incubation:
                    numtests = 0;
                    stringnum = filelines.SkipWhile(x => !x.Contains("Number of Tests to run:")).ToList();
                    numtests = stringnum.Count == 0 ? 1 : Int32.Parse(stringnum[0].Substring(stringnum[0].IndexOf(": ") + 1));

                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("Average") && filelines[i].Contains("1.1;"))
                        {
                            stringnum = filelines.SkipWhile(x => !x.Contains("Average")).ToList();
                            numtests = stringnum.Count == 0 ? 1 : Int32.Parse(stringnum[0].Substring(stringnum[0].IndexOf(": ") + 1));
                        }
                        
                    }
            
            //if (numtests == 1)
            //{
            //    for (int i = 0; i < filelines.Count; i++)
            //    {
            //        if (filelines[i].Contains("Incubation Start") && filelines[i].Contains("1.1;"))
            //        {
            //            startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
            //        }
            //        if (filelines[i].Contains("Incubation Finished") || (filelines[i].Contains("Incubation End") && filelines[i].Contains("1.1;")))
            //        {
            //            endline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);

            //            starttime = DateTime.Parse(startline);
            //            endtime = DateTime.Parse(endline);

            //            if (starttime > endtime)
            //            {
            //                timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
            //            }
            //            else
            //            {
            //                timediff += endtime - starttime;
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    for (int n = 1; n <= numtests; n++)
            //    {
            //        for (int i = 0; i < filelines.Count; i++)
            //        {
            //            if (filelines[i].Contains(string.Format("T{0}", n)))
            //            {
            //                if (filelines[i].Contains("Incubation Start") && filelines[i].Contains("#1.1;"))
            //                {
            //                    startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
            //                }
            //                if (filelines[i].Contains("Incubation Finished"))
            //                {
            //                    endline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);

            //                    starttime = DateTime.Parse(startline);
            //                    endtime = DateTime.Parse(endline);

            //                    if (starttime > endtime)
            //                    {
            //                        timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
            //                    }
            //                    else
            //                    {
            //                        timediff += endtime - starttime;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            break;
                case WorklistActionType.ProbeWash:
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("ProbeWashAction"))
                        {
                            startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                            starttime = DateTime.Parse(startline);

                            endline = filelines[i + 1].Substring(0, filelines[i + 1].IndexOf(_delimiter) + 1);
                            endtime = DateTime.Parse(endline);

                            if (starttime > endtime)
                            {
                                timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                            }
                            else
                            {
                                timediff += endtime - starttime;
                            }
                        }
                    }
                    break;
                case WorklistActionType.CaptureWellImage:
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("CaptureWellImageAction"))
                        {
                            startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                            starttime = DateTime.Parse(startline);
                        }
                        if (filelines[i].Contains("EvaluateImage"))
                        {
                            endline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                            endtime = DateTime.Parse(endline);

                            if (endtime < starttime)
                            {
                                timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                            }
                            else
                            {
                                timediff += endtime - starttime;
                            }
                        }
                    }
                    break;
                case WorklistActionType.NeedleDecon:
                    for (int i = 0; i < filelines.Count; i++)
                    {
                        if (filelines[i].Contains("NeedleDecontaminateAction"))
                        {
                            startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                            endline = filelines[i + 1].Substring(0, filelines[i + 1].IndexOf(_delimiter) + 1);

                            DateTime.TryParse(startline, out starttime);
                            if (!DateTime.TryParse(endline, out endtime))
                            {
                                endline = filelines[i + 2].Substring(0, filelines[i + 2].IndexOf(_delimiter) + 1);
                                DateTime.TryParse(endline, out endtime);
                            }

                            if (endtime < starttime)
                            {
                                timediff += (endtime + TimeSpan.FromDays(1)) - starttime;
                            }
                            else
                            {
                                timediff += endtime - starttime;
                            }
                            
                        }
                    }
                    break;
           
            }

            return timediff;
        }

        public List<Tuple<string, TimeSpan>> GetFluidTransferActionFromWorklist(List<string> filelines, string testName)
        {
            List<Tuple<string, TimeSpan>> fluidTransferAction = new List<Tuple<string, TimeSpan>>();
            List<string> actionnames = new List<string>();
            TimeSpan actiontime = TimeSpan.Zero;

            foreach (var line in filelines)
            {
                if (line.Contains("FluidTransferAction"))
                {
                    var name = line.Substring(line.IndexOf("FluidTransferAction; "), line.IndexOf(", ") - line.IndexOf("FluidTransferAction; "));
                    if (!actionnames.Any(x => x.Equals(name)))
                    {
                        actionnames.Add(name);
                    }
                }
            }

            string startline;
            string endline;
            DateTime starttime;
            DateTime endtime;

            foreach (var actionname in actionnames)
            {
                for (int i = 0; i < filelines.Count; i++)
                {
                    if (filelines[i].Contains(actionname) && filelines[i].Contains(testName))
                    {
                        startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                        endline = filelines[i + 1].Substring(0, filelines[i + 1].IndexOf(_delimiter) + 1);

                        starttime = DateTime.Parse(startline);
                        if (!DateTime.TryParse(endline, out endtime))
                        {
                            endline = filelines[i + 2].Substring(0, filelines[i + 2].IndexOf(_delimiter) + 1);
                            DateTime.TryParse(endline, out endtime);
                        }
                        if (endtime == starttime)
                        {
                            endline = filelines[i + 2].Substring(0, filelines[i + 2].IndexOf(_delimiter) + 1);
                            DateTime.TryParse(endline, out endtime);
                        }
                        if (endtime < starttime)
                        {
                            actiontime += (endtime + TimeSpan.FromDays(1)) - starttime;
                        }
                        else
                        {
                            actiontime += endtime - starttime;
                        }
                    }
                }
                fluidTransferAction.Add(new Tuple<string, TimeSpan>(actionname, actiontime));
                actiontime = TimeSpan.Zero;
            }

            return fluidTransferAction;
        }
        
        public List<Tuple<string, TimeSpan>> GetMultiShotActionTimeFromWorklist(List<string> filelines, string testName)
        {
            List<Tuple<string, TimeSpan>> multiShotTransferAction = new List<Tuple<string, TimeSpan>>();
            List<string> actionnames = new List<string>();
            TimeSpan actiontime = TimeSpan.Zero;

            foreach (var line in filelines)
            {
                if (line.Contains("MultiShotFluidTransferAction"))
                {
                    var name = line.Substring(line.IndexOf("MultiShotFluidTransferAction"), line.IndexOf(", ") - line.IndexOf("MultiShotFluidTransferAction"));
                    if (!actionnames.Any(x => x.Equals(name)))
                    {
                        actionnames.Add(name);
                    }
                }
            }

            string startline;
            string endline;
            DateTime starttime;
            DateTime endtime;

            foreach (var actionname in actionnames)
            {
                for (int i = 0; i < filelines.Count; i++)
                {
                    if (filelines[i].Contains(actionname) && filelines[i].Contains(testName))
                    {
                        startline = filelines[i].Substring(0, filelines[i].IndexOf(_delimiter) + 1);
                        endline = filelines[i + 1].Substring(0, filelines[i + 1].IndexOf(_delimiter) + 1);

                        starttime = DateTime.Parse(startline);
                        if (!DateTime.TryParse(endline, out endtime))
                        {
                            endline = filelines[i + 2].Substring(0, filelines[i + 2].IndexOf(_delimiter) + 1);
                            DateTime.TryParse(endline, out endtime);
                        }
                        if (endtime == starttime)
                        {
                            endline = filelines[i + 2].Substring(0, filelines[i + 2].IndexOf(_delimiter) + 1);
                            DateTime.TryParse(endline, out endtime);
                        }
                        if (endtime < starttime)
                        {
                            actiontime += (endtime + TimeSpan.FromDays(1)) - starttime;
                        }
                        else
                        {
                            actiontime += endtime - starttime;
                        }
                    }
                }
                multiShotTransferAction.Add(new Tuple<string, TimeSpan>(actionname, actiontime));
                actiontime = TimeSpan.Zero;
            }

            return multiShotTransferAction;
        }
    }
}
