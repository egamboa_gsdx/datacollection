﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSD.TestLab.DataCollection.Models
{
    public class FileHistory
    {
        public FileHistory(string fullInstrumentPath)
        {
            FullInstrumentPath = fullInstrumentPath;
            WorklistFullPath = fullInstrumentPath.Substring(0, fullInstrumentPath.IndexOf(","));

            DatabaseFullPath = fullInstrumentPath.Substring(fullInstrumentPath.IndexOf(",") + 2);
            DatabaseFileName = !DatabaseFullPath.Equals("NoDb") ? Path.GetFileName(DatabaseFullPath) : "NoDb";
            LastAccessed = DateTime.Now;
        }

        public string FullInstrumentPath { get; set; }
        public string WorklistFullPath { get; set; }
        public string DatabaseFullPath { get; set; }
        public string DatabaseFileName { get; set; }
        public DateTime LastAccessed { get; set; }

        public override bool Equals(object? obj)
        {
            if (obj == null)
            {
                return false;
            }

            return FullInstrumentPath == (obj as FileHistory)?.FullInstrumentPath; ;
        }

        public override int GetHashCode()
        {
            return FullInstrumentPath.GetHashCode();
        }
    }
}
