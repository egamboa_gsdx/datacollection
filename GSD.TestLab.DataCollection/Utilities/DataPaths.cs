﻿#pragma warning disable SA1300

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Threading.Tasks;

namespace GSD.TestLab.DataCollection.Utilities
{
    public static class DataPaths
    {
        public static string ProgramLocalAppData => DirectoryExists(_programLocalAppData);
        private static string _programLocalAppData => (Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
            Path.DirectorySeparatorChar + "GSD Test Lab" + Path.DirectorySeparatorChar + "Monthly System Performance");
        
        /// <summary>
        /// Json file for previous used Status Log File & DB
        /// </summary>
        public static string FileHistoryPath => _fileHistoryPath;
        private static string _fileHistoryPath => (ProgramLocalAppData + Path.DirectorySeparatorChar + "MonthlyInstrumentFileHistory.json");
        
        private static string DirectoryExists(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            return directory;
        }
    }
}
